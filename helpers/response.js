const response = (res, data, success) => {
    res.send({ success, data });
}   

module.exports = {
    response   
};