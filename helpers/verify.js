const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config/jwt_secret');
const response = require('./response');

const verifyToken = async(req, res, next) => {

    const token = req.header('access-token');
    if(!token) {
        return response.response(res, 'No Token Provided', 0);
    }

    try {
        const decoded = jwt.verify(token, config.jwt_secret);
        const user = await User.findOne({ _id: decoded.id });
        req.user = user;
        next();
    } catch (error) {
        console.log('Invalid Token');
        response.response(res, 'Invalid Token Provided', 0);
    }

}

module.exports = verifyToken;