const mongoose = require("mongoose");
const config = require("../config/db");
const dbURI = config;
const connectDB = async () => {
  try {
    await mongoose.connect(dbURI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });

    console.log(`MongoDB connected on ${dbURI}`);
  } catch (err) {
    console.log(err.message);
    // Exits process with the failure
    process.exit(1);
  }
};

module.exports = connectDB;