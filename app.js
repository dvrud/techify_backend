const express = require('express');
const bodyParser = require('body-parser');
const connectDb = require('./helpers/db');
const cors = require('cors')
const app = express();

// Middleware for bodyparser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Handling cors error
app.use(cors())

// Connecting with mongodb
connectDb();

// Connecting the routes
app.use('/', require('./routes/user'));

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server started on ${PORT}`);
})

module.exports = app;



