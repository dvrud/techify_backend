const express = require("express");
const response = require("../helpers/response");
const User = require("../models/user");
const { check, validationResult } = require("express-validator");
const passwordValidator = require("password-validator");
const bcrypt = require('bcryptjs');
const config = require('../config/jwt_secret');
const jwt = require('jsonwebtoken');
const verify = require('../helpers/verify');
const router = express.Router();

router.get('/test', (req, res) => {
  response.response(res, 'wordfsks !')
})

router.post(
  "/createUser",
  [
    check("email").isEmail(),
    check("name").notEmpty(),
    check("password").notEmpty(),
    check("address").notEmpty(),
    check("phone").notEmpty()
  ],
  async (req, res) => {
    try {
      const { name, email, password, address, phone } = req.body;
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        response.response(res, errors.array(), 0);
      } else {
        const schema = new passwordValidator();
        schema
          .is()
          .min(8)
          .is()
          .max(20)
          .has()
          .uppercase()
          .has()
          .lowercase()
          .has()
          .digits()
          .has()
          .symbols()
          .has()
          .not()
          .spaces()
          .is()
          .not()
          .oneOf(["password", "12345678"]);
          if(!schema.validate(password)) {
              return response.response(res, 'Password should be a combination of uppercase, lowercase, symbols and numbers and should be greater than 8 characters !', 0)
          }

          const newUser = {
              name, email, password, address, phone
          }
          const salt = await bcrypt.genSalt(10);
          newUser.password = await bcrypt.hash(password, salt);

          const createdUser = await new User(newUser).save();
          console.log(createdUser)
          response.response(res, 'Registered Succesfully and can Login !', 1)


      }
    } catch (error) {
      console.log(error, "Error in creating the user");
      return response.response(res, "Error in creating the user", 0);
    }
  }
);

router.post("/login", [check('email').isEmail(), check('password').notEmpty()],async (req, res) => {
    const { email, password } = req.body;
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return response.response(res, 'Seems like you are missing some field, Please Enter email and password', 0)
    }

    const user = await User.findOne({ email });
    if(!user) {
        return response.response(res, 'Invalid Credentials', 0);
    }
    const validate = await bcrypt.compare(password, user.password);
    if(!validate) {
        return response.response(res, 'Invalid Credentials', 0)
    }

    const payload = {
      id: user._id
    }

    const token = await jwt.sign(payload, config.jwt_secret, { expiresIn: config.jwt_expiry });
    
    delete user._doc.password;
    console.log(token, 'this is token', user.password);
    const finalData = {
      ...user._doc, token
    }



  return response.response(res, finalData, 1);
});

router.post('/user', verify, async(req, res) => {
  response.response(res, req.user, 1);
});

router.post('/users', verify, async(req, res) => {
  if(!req.user.admin) {
    return response.response(res, 'Not Authorized !', 0)
  }
  let users = await User.find();
  users = users.filter(user => !user._id.equals(req.user._id));
  response.response(res, users, 1);
  
});

module.exports = router;
